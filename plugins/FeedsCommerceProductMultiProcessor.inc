<?php
/**
 * @file
 * Class definition of FeedsCommerceProductMultiProcessor.
 *
 * @todo Allow differentiation of identically named options in mapper (perhaps
 *   show each bundle a field's attached to?)
 *
 * @see FeedsCommerceProductProcessor.inc
 */

/**
 * Creates products of different types from feed items.
 *
 * @see FeedsCommerceProductProcessor
 */
class FeedsCommerceProductMultiProcessor extends FeedsCommerceProductProcessor {

  /**
   * {@inheritdoc}
   */
  public function newEntity(FeedsSource $source) {
    // Wrap parent::newEntity(), modifying the bundle setting if necessary.

    $bundle = $this->getBundle($source);
    if ($bundle) {
      // Temporarily change $this->config['bundle'] to influence
      // parent::newEntity().
      $default = $this->config['bundle'];
      $this->config['bundle'] = $bundle;
    }

    $entity = parent::newEntity($source);

    if ($bundle) {
      // Revert to the old value.
      $this->config['bundle'] = $default;
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function process(FeedsSource $source, FeedsParserResult $parser_result) {
    // Keep track of the parser's result so that newEntity() can use it to read
    // the bundle.
    $this->commerce_feedsmulti_result = $parser_result;
    parent::process($source, $parser_result);
    $this->commerce_feedsmulti_result = NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function configForm(&$form_state) {
    $form = parent::configForm($form_state);
    $form['bundle']['#title'] = t("Default product type");
    $form['bundle']['#description'] = t('Select the default product type for the products to be created. If a product type isn\'t provided by the source or is invalid this will be used. <strong>Note:</strong> Users with "import !feed_id feeds" permissions will be able to <strong>import</strong> products of the type selected here regardless of the product level permissions. Further, users with "clear !feed_id permissions" will be able to <strong>delete</strong> imported products regardless of their product level permissions.', array('!feed_id' => $this->id));
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingTargets() {

    $targets['bundle'] = array(
      'name' => t("Product type"),
      'description' => t("The type of the product"),
      'callback' => 'commerce_feedsmulti_blank_callback',
      'summary_callbacks' => array(),
    );

    $targets += parent::getMappingTargets();

    // Loop through every product type to get all possible mapping targets. This
    // would cause problems if targets could vary between bundles (I don't know
    // of that being possible).
    $entity_type = $this->entityType();
    $bundles = commerce_product_types();
    foreach ($bundles as $bundle => $info) {
      if ($bundle != $this->config['bundle']) {
        $new_targets = module_invoke_all('feeds_processor_targets', $entity_type, $bundle);
        drupal_alter('feeds_processor_targets', $new_targets, $entity_type, $bundle);
        $targets += $new_targets;
      }
    }

    return $targets;
  }

  /**
   * Read the current FeedsParserResult item and return the bundle value if
   * present and valid.
   *
   * IMPORTANT: It should only be called from within process().
   *
   * @param FeedsSource $source
   *   The feeds source that will create the product
   * @return bool|string
   *   The bundle name if present and valid; FALSE otherwise
   */
  private function getBundle(FeedsSource $source) {
    $mappings = $source->importer()->config['processor']['config']['mappings'];
    foreach ($mappings as $mapping) {
      if ($mapping['target'] == 'bundle') {
        $result = $this->commerce_feedsmulti_result;
        $bundle = $this->getSourceValue($source, $result, $mapping['source']);
        // Don't trust the input to be a valid product type.
        if (!$bundle || !$this->isBundleValid($bundle)) {
          $bundle = FALSE;
        }
        return $bundle;
      }
    }

    return FALSE;
  }

  /**
   * Check if the bundle is a valid product type.
   *
   * @param string $bundle
   *   The bundle name
   * @return bool
   *   TRUE if the product type is valid; FALSE otherwise
   */
  private function isBundleValid($bundle) {
    $types = commerce_product_types();
    return isset($types[$bundle]);
  }
}

/**
 * This is used as the product type's setter callback - it's just a noop.
 */
function commerce_feedsmulti_blank_callback() {
}
