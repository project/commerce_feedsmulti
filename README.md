# Commerce Feeds multitype

*This module requires Commerce Feeds to be patched from [#2635548].* (More
details: [#2016561].)

Commerce Feeds multitype provides an additional Feeds processor that can create
different types of products in one import. It provides mapping targets for the
product type and any fields attached to any product type. If you're updating
an existing product, any product type data in the source will be ignored.

[#2635548]: https://drupal.org/node/2635548
[#2016561]: https://drupal.org/node/2016561
